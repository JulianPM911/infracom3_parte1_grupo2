# Importar librerias para el manejo del sistema, algoritmos de hash, tiempos, captura de paquetes, manejo de threads, manejo de sockets y funciones definidas en el archivo utils
import socket
import os
import threading
from Utils import diagestHash, write_log_file
import time
from datetime import datetime
import pyshark

# Constantes SEPARATOR, BUFFER_SIZE, HOST y PORT
SEPARATOR = "<SEPARATOR>"
BUFFER_SIZE = 4096
HOST = '192.168.0.23'
PORT = 9879

# Contador del numero de threads ejecutandose
contador = 0

# Funcion que correr cada thread la cual consiste en recibir el archivo, el hash y la recepcion de confirmacion de integridad
# Por cada thread que se corre se genera un archivo log que contiene la informacion respectiva detalla segun las especificaciones de la guia
def correr_clientes(ClientSocket):

    # Contador global del numero de threads
    global contador 
    contador+=1

    # Captura de paquetes por la interfaz de ethernet 
    capture = pyshark.LiveCapture(interface='Ethernet')
    capture.sniff(timeout=1)

    # Tiempo de incio de ejecucion
    start_time = datetime.now()

    # Entablar conexiones con el el servidor para empezar la transferencia del archivo
    try:
        ClientSocket.connect((HOST, PORT))
        mensaje = "listo"
        ClientSocket.send(str.encode(mensaje))
        received = ClientSocket.recv(BUFFER_SIZE).decode()

        # Variables correspondientes al nombre del archivo y tamanio del archivo
        filename, filesize = received.split(SEPARATOR)
        filename = os.path.basename(filename)
        filesize = int(filesize)

        # Escritura del archivo recibido por partes desde el servidor
        with open('./Client/ArchivosRecibidos/'+ filename, "wb") as f:
            while True:
                bytes_read = ClientSocket.recv(BUFFER_SIZE)
                if  ("fin" in bytes_read.decode()) or ((bytes_read) is None) or (not bytes_read):    
                    sin_fin=(bytes_read)[:-3]
                    f.write(sin_fin)
                    break
                f.write(bytes_read)

        # Generacion del hash en base del archivo recibido
        hashRecibido = ClientSocket.recv(BUFFER_SIZE)
        archivo = open('./Client/ArchivosRecibidos/'+filename, "rb")
        hashCalculado = diagestHash(archivo)
        archivo.close()

        # Validacion del codigo de hash genrado con el recibido por parte del servidor para verificar la integridad dde este
        integridad = "La integridad del archivo no se ve afectada en la transferencia" if (hashRecibido == hashCalculado) else "La integridad se vió afectada"
        ClientSocket.send(integridad.encode())
        confirmacion = ClientSocket.recv(BUFFER_SIZE).decode()
        print(confirmacion)

        # Tiempo final de ejecucion
        finish_time = datetime.now()

        # Escritura del archivo log con la informacion establecida en la guia
        numeropack=str(capture).replace("<LiveCapture (","").replace(" packets)>","")
        write_log_file(filename, filesize, contador, confirmacion, start_time, finish_time, int(numeropack), BUFFER_SIZE)

        # Cerrar la conexion del socket
        ClientSocket.close()
    except socket.error as e:
        print(str(e))
        ClientSocket.close()
    
# Ingresar el numero de clientes a establecer conexion
numClientes = int(input("Ingrese el número de clientes:"))

# Contador del numero de threads local para el while
ThreadCount = 0

# Creacion de sockets asociados a los threads y iniciar el metodo correr_cliente de cada thread
while ThreadCount < numClientes:
    Client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    threading.Thread(target=correr_clientes, args=(Client, )).start()
    time.sleep(1)
    ThreadCount += 1