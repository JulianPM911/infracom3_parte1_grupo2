# Importar librerias para el manejo del sistema, algoritmos de hash, tiempos, captura de paquetes y asincronia
import os
import hashlib
import datetime
import pyshark
import asyncio

# Funcion para imprimir los archivos diponibles en el servidor (prueba100MB y prueba250 MB)
def print_files():
    files_server = os.listdir('./files/')
    print("Los archivos que estan disponibles en el servidor son:")
    for i in range (len(files_server)):
        print(str(i+1) + "." + files_server[i])

# Funcion para obtener el archivo dado el numero ingresado como parametro
def get_file (num_file):
    archivo = ''
    if num_file == "1":
        archivo = './files/prueba100MB.txt'
    elif num_file == "2":
        archivo = './files/prueba250MB.txt'
    return archivo

# Funcion para generar el archivo de log en el directorio llamado Logs con la informacion determinada en la guia
def write_log_file (file_name, file_size, num_client, status_file, start_time, finish_time):
    today = datetime.datetime.now()
    name_file = str(today.year) + "-" + str(today.month) + "-" + str(today.day) + "-" + str(today.hour) + "-" + str(today.minute) + "-" + str(today.second) + "-" + str(today.microsecond) + "-log.txt"
    log_file = open("./Logs/" + name_file, "w")
    log_file.write("Nombre del archivo: " + file_name.split("/")[2] + "\n")
    log_file.write("Tamanio del archivo: " + str(file_size) + "\n")
    log_file.write("Cliente que realiza la transeferencia de archivos: " + str(num_client) + "\n")
    log_file.write("Estado de entrega del archivo: " + str(status_file) + "\n")
    log_file.write("Tiempo de transferencia: " + str(finish_time - start_time) + "\n")

# Funcion para generar el valor de hash del archivo trasmitido (Algoritmo de hash utilizado SHA-512)
def diagestHash (file):
    m = hashlib.sha512()
    m.update(file.read())
    return bytes(m.hexdigest(), 'utf-8')

# Arreglo donde se almacenan los paquetes 
packet_list = []

# Funcion que almacena cada paquete dentro del arreglo de paquetes con la informacion de la version del paquete, capa de trasporte y el tamanio del paquete
def process_packets(packet):
    global packet_list
    try:
        packet_version = packet.layers[1].version
        layer_name = packet.layers[2].layer_name
        packet_list.append(f'{packet_version}, {layer_name}, {packet.length}')
    except AttributeError:
        pass

# Funcion para la captura de paquetes haciendo uso de la libreria de pyshark sobre la interfaz ens33
def capture_packets(timeout):
    capture = pyshark.LiveCapture(interface='ens33')
    try:
      capture.apply_on_packets(process_packets, timeout=timeout)
    except asyncio.TimeoutError:
        pass
    finally:
        global packet_list
        return len(packet_list)